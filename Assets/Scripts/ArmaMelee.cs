﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmaMelee : Arma {
    
    public UnidadEstadisticas.tipoDaño tipo = UnidadEstadisticas.tipoDaño.Fisico;




    public TrailRenderer trail;

    [SerializeField]private bool atacando;


   

    public override void AnimDeAtaque()
    {
       // throw new NotImplementedException();
    }

    public override void AnimPostAtaque()
    {
        DesactivarTrail();
    }

    public override void AnimPreAtaque()
    {
        ActivarTrail();
    }


    public void ActivarTrail()
    {
        if(trail != null)
            trail.enabled = true;
    }


    public void DesactivarTrail()
    {
        if (trail != null)
            trail.enabled = false;
    }

    public override void AtaqueIniciado()
    {
        AnimPreAtaque();
        atacando = true;
    }


    public override void AtaqueTerminado()
    {
        AnimPostAtaque();
        atacando = false;
    }

    private void OnTriggerStay(Collider col)
    {
        //Debug.Log(col.name);
        if(col.gameObject == objetivo && atacando)
        {
           // Debug.Log("Encontrado: "+ col.name);
            atacando = false;
            col.GetComponent<UnidadBatalla>().RecibirDaño(daño, tipo, transform.position);
        }
    }    
    
    


}
