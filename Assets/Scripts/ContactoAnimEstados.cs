﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactoAnimEstados : MonoBehaviour {

    public UnidadBatalla ub;
    public ControladorDeEstados estados;

    public Arma arma;


    private void Awake()
    {
        estados = ub.estados;
    }


    public void AtaqueTerminado()
    {
        estados.AtaqueTerminado();
        AnimPostAtaque();
    }

    public void AtaqueIniciado()
    {
        arma.AtaqueIniciado();
        AnimPreAtaque();
    }

    public void AnimPreAtaque()
    {
        if (arma != null)
            arma.AnimPreAtaque();
    }

    public void AnimDeAtaque()
    {
        if (arma != null)
            arma.AnimDeAtaque();
    }

    public void AnimPostAtaque()
    {
        if (arma != null)
            arma.AnimPostAtaque();
    }

}
