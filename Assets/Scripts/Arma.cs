﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Arma : MonoBehaviour {

    public int daño;
    public GameObject objetivo;
    public SpriteRenderer render;

    public Animator anim;
    public int layerObjetivo;

    abstract public void AnimPreAtaque();

    abstract public void AnimDeAtaque();

    abstract public void AnimPostAtaque();

    abstract public void AtaqueTerminado();

    abstract public void AtaqueIniciado();

    public void SetDaño(int n)
    {
        daño = n;
    }

    public void SetObjetivo(GameObject objetivo)
    {
        this.objetivo = objetivo;
    }

}
