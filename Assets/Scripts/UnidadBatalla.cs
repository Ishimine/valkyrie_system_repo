﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnidadBatalla : MonoBehaviour {
    UnidadMovimiento mov;
    private bool vivo = true;
    private Escuadron escuadron;
    Rigidbody rb;
    Collider col;
    private int idDePosicion;

    public GameObject cuerpo;

    private Transform posOrigen;
    public float distAtaque = 3;

    private Vector3 vel;
    public float velSuavizado;
    public float velMax;
    public float radioAreaLlegada = 1;

    private UnidadBatalla objetivo;  


    [SerializeField] public UnidadEstadisticas estadisticas;
    [SerializeField] public ControladorDeEstados estados;
    [SerializeField] private Combo combo;


    public GameObject objeto_1;
    public GameObject objeto_2;

    public ContactoAnimEstados animEstados;

    public Arma arma;

    public float fGolpeado = 5;


   [SerializeField] private bool mirandoDer;
    public bool MirandoDer
    {
        get
        {
            return mirandoDer;
        }
        set
        {
            if (!value)
            {
                cuerpo.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else
            {
                cuerpo.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            mirandoDer = value;
        }        
    }
    
    public delegate void ActPj(UnidadBatalla u);
    public ActPj PjAtacado;

    public delegate void Trigger();
    public Trigger sinAcciones;



    private void Awake()
    {
        //mov = GetComponent<UnidadMovimiento>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        estadisticas.UnidadMuerta = UnidadMuerta;
        CargarArma();
        //estadisticas.ReiniciarEstadisticas();
    }

    public bool EstaViva()
    {
        return vivo;
    }
    public int GetId()
    {
        return idDePosicion;
    }
    public void SetId(int x)
    {
        idDePosicion = x;
    }
    internal void PosicionarEn(Vector3 pos)
    {
        transform.position = pos;
    }


    public void InicializarUnidad() //Inicializa la unidad al iniciar el turno
    {
        combo.InicializarCombo();
    }
   
  

    public void CargarArma(/*GameObject n*/)
    {
        GameObject clone = Instantiate<UnityEngine.Object> (Resources.Load("Armas/Espada"),objeto_1.transform) as GameObject;
        arma = clone.GetComponent<Arma>();
        animEstados.arma = arma;
        arma.SetDaño(estadisticas.GetAtaque());
    }

    // Version Previa
    IEnumerator Movimiento(Transform obj,bool atacar)
    {
        Vector3 dif = Vector3.zero;
        Vector3 pos = new Vector3(obj.position.x, transform.position.y, obj.position.z);
        if (atacar)
        {
            dif = distAtaque * Vector3.right;
            if (pos.x - transform.position.x < 0)
                dif *= -1;
        }
        pos += pos - dif;

        pos = new Vector3(obj.position.x, transform.position.y, obj.position.z) - dif;
        while (Vector3.Distance(transform.position, pos) > radioAreaLlegada)
        {
            pos = new Vector3(obj.position.x, transform.position.y, obj.position.z) - dif;
            if (pos.x - transform.position.x < 0) transform.rotation = Quaternion.Euler(0, 180, 0);
            else transform.rotation = Quaternion.Euler(0, 0, 0);



            transform.position = Vector3.SmoothDamp(transform.position,pos,ref vel, velSuavizado, velMax);
            estados.SetVel(vel.sqrMagnitude);
            yield return null;
        }
        if (atacar)
        {
            Atacar();
            StopAllCoroutines();
            MirarObjetivo();
            //Debug.Log("Rutina Iniciada: Volviendo");
            StartCoroutine(EsperaPostAtaque());
        }
        else
        {
            MirarAdelante();

            if(combo.terminado)
            {
                if (sinAcciones != null) sinAcciones();
            }

            //Debug.Log("Rutina Termiinada");
            //StopAllCoroutines();
        }

        estados.SetVel(0);
        estados.SetEnMovimiento(false);
    }
    
    IEnumerator EsperaPostAtaque()
    {
        //Debug.Log("EsperaPostAtaque");
        yield return new WaitForSeconds(3f);
        ControlladorDeBatalla.instance.UnidadAtaqueTerminado(this);
        MoverHasta(posOrigen, false);
    }

    private void MirarObjetivo()
    {
        if (objetivo.transform.position.x - transform.position.x > 0)
            transform.rotation = Quaternion.Euler(0, 0, 0);
        else
            transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    private void MirarAdelante()
    {
      //  Debug.Log("MirarAdelante:" + mirandoDer);
        if (mirandoDer) transform.rotation = Quaternion.Euler(0, 0, 0);
        else transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    public void MoverHasta(Transform obj, bool x)
    {
        estados.SetEnMovimiento(true);
        StopAllCoroutines();
        StartCoroutine(Movimiento(obj, x));
    }
        

    public void SetNewPosition(Transform x)
    {
        posOrigen = x;
    }

    public void Atacar(UnidadBatalla obj)
    {
        if(estados.Atacando() || combo.terminado)
        {
            return;
        }

        objetivo = obj;
        arma.SetObjetivo(obj.gameObject);
        arma.SetDaño( estadisticas.GetAtaque());


        MoverHasta(obj.transform, true);
        estados.AtaqueIniciado();


    }

    private void Atacar()
    {        
        estados.Atacar(combo.SigAtaque());
    }   

    public void SetLayer(string x,string y, string z)
    {
        gameObject.layer = LayerMask.NameToLayer(x);
        arma.gameObject.layer = LayerMask.NameToLayer(y);
        arma.layerObjetivo = LayerMask.NameToLayer(z);
    }

    public void SetEscuadron(Escuadron e)
    {
        escuadron = e;
    }

    public void SetObjetivo(UnidadBatalla unidad)
    {
        objetivo = unidad;
    }

    public void UnidadMuerta()
    {
        ControlladorDeBatalla.instance.UnidadAtaqueTerminado(this);
        vivo = false;
        escuadron.ReportarUnidadMuerta(this);
        estados.Muerto();
        StopAllCoroutines();
    }

    public void RecibirDaño(int daño, UnidadEstadisticas.tipoDaño tipo, Vector3 pos)
    {
        estadisticas.RecibirDaño(daño, tipo);
        estados.KnockBackDebil();
        MirarDir(pos.x);
        StopAllCoroutines();
        if(vivo)
            StartCoroutine(EsperaPostAtaque());
        if (PjAtacado != null)      //Informa que el pj fue atacado y envia una referencia del mismo
        {
            PjAtacado(this);
        }
    }
    
    public void AplicarKnockBack(Vector3 pos)
    {
        float dir = Mathf.Sign(pos.x - transform.position.x);
        Vector2 vDir = new Vector2(dir, 1);
        rb.velocity = (vDir * fGolpeado);
        Debug.Log(vDir * fGolpeado);
        Debug.Log(rb.velocity);
    }


    private void MirarDir(float x)
    {
        if( x - transform.position.x < 0)        
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void AplicarCapaEnArma(int capa)
    {
        if (arma != null)
            arma.render.sortingOrder = capa;
    }


    public void ActualizarVida()
    {
        print("kkkkk");
    }

}
