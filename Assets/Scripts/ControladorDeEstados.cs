﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ControladorDeEstados {

    [SerializeField] public Animator anim;

    private bool atacando;
    private bool muerto;

    public bool Atacando()
    {
        return atacando;
    }

    public void AtaqueTerminado()
    {
        atacando = false;
    }

   
	public void Atacar(int x)
    {   
        anim.SetInteger("idAtkAct", x);
        anim.SetTrigger("Atacar");
    }

    public void AtaqueIniciado()
    {
        atacando = true;
    }

    public void KnockBackDebil()
    {
        anim.SetTrigger("KnockBackDebil");
    }

    public void SetEnMovimiento(bool x)
    {
        anim.SetBool("EnMovimiento",x);
    }

    public void SetVel(float vel)
    {
        anim.SetFloat("Vel",vel);
    }
    public void Muerto()
    {
        muerto = true;
        anim.SetBool("Muerto", true);
        SetEnMovimiento(false);        
    }

    public void Revivido()
    {
        muerto = false;
        anim.SetTrigger("Revivido");
    }
}
